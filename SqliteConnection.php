<?php


namespace Packages\DbAdapter;


use SQLiteDatabase;

class SqliteConnection implements ConnectionInterface
{
    protected $sqlite;
    protected $file;

    public function  __construct($file)
    {
        $this->file = $file;
    }

    public function link(): object
    {
        $this->sqlite = new SQLiteDatabase($this->file);
        return $this->sqlite;
    }

    public function sendQuery($query)
    {
        $result =  $this->link()->query($query);
        return $result;
    }

    public function __destruct()
    {
        if ($this->sqlite) $this->link()->close();
    }

    public function one($result)
    {
        return sqlite_fetch_array($result, SQLITE_ASSOC);
    }

    public function all($result)
    {
        return sqlite_fetch_all($result, SQLITE_ASSOC);
    }
}