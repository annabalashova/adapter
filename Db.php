<?php


namespace Packages\DbAdapter;

use mysqli;
class Db
{
    protected static $instance;
    /**
     * @var ConnectionInterface;
     */
    protected $connection;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    protected function __construct()
    {
    }

    protected function __clone()
    {
    }

    /**
     * @param mixed $connection
     */
    public function setConnection(ConnectionInterface $connection): void
    {
        $this->connection = $connection;
    }

    public function link()
    {
        $this->connection->link();
    }

    public function sendQuery($query)
    {
      return $this->connection->sendQuery($query);

    }

    public function one($result)
    {
        return $this->connection->one($result);
    }

    public function all($result)
    {
        return $this->connection->all($result);
    }

     public function __destruct()
     {
         if ($this->connection) $this->connection->link()->close();
     }


}

