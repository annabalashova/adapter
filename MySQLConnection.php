<?php


namespace Packages\DbAdapter;


class MySQLConnection implements ConnectionInterface
{
    protected $config = [];
    protected $mysqli;

    public function __construct($config = [])
    {
        {
            $this->config['host'] = $config['host'];
            $this->config['user'] = $config['user'];
            $this->config['pass'] = $config['pass'];
            $this->config['db'] = $config['db'];
        }

    }

    public function link(): object
    {
        {
            if (!$this->mysqli) {
                $this->mysqli = new \mysqli(
                    $this->config['host'],
                    $this->config['user'],
                    $this->config['pass'],
                    $this->config['db']);
                if ($this->mysqli->connect_error) {
                    die('Ошибка подключения (' . $this->mysqli->connect_errno . ') '
                        . $this->mysqli->connect_error);
                }
                $this->mysqli->set_charset("utf8");
            }
            return $this->mysqli;
        }
    }

    public function sendQuery($query)
    {
        $result = $this->link()->query($query);
        return $result;
    }

    public function __destruct()
    {
        if ($this->mysqli) $this->link()->close();
    }

    public function one($result)
    {
         return mysqli_fetch_assoc($result);
    }

    public function all($result)
    {
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }
}
