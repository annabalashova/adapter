<?php


namespace Packages\DbAdapter;


interface ConnectionInterface
{
    public function  __construct($config);

    public function link() : object;

    public function sendQuery($query);

    public function one($result);

    public function all($result);
}