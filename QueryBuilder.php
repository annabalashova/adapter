<?php


namespace Packages\DbAdapter;

use mysqli;
class QueryBuilder implements QueryBuilderInterface
{
    protected $db;

    public $columns = ['*'];
    public $conditions = [];
    public $table;
    public $limit;
    public $offset;
    public $order = [];

    public function __construct(\Packages\DbAdapter\Db $db)
    {
        $this->db = $db;
    }

    public function select($columns): QueryBuilderInterface
    {
        $this->columns = $columns;
        return $this;
    }

    public function where($conditions): QueryBuilderInterface
    {
        $this->conditions = $conditions;
        return $this;
    }

    public function table($table): QueryBuilderInterface
    {
        $this->table = $table;
        return $this;
    }

    public function limit($limit): QueryBuilderInterface
    {
        $this->limit = $limit;
        return $this;
    }

    public function offset($offset): QueryBuilderInterface
    {
        $this->offset = $offset;
        return $this;
    }

    public function order($order): QueryBuilderInterface
    {
        $this->order = $order;
        return $this;
    }

    public function build(): string
    {
        if (!$this->table) {
            throw new \Exception('таблица не задана');
        }
        $str =  'SELECT ' . implode(', ', $this->columns)
            . ' FROM ' . $this->table . ' WHERE ' . implode('', array_keys($this->conditions)) . ' = '
            . "'" . implode('', array_values($this->conditions)) . "'" . ' ORDER BY '
            . implode('', array_keys($this->order)) . ' ' . implode('', array_values($this->order))
            . ' LIMIT ' . $this->limit . ' OFFSET ' . $this->offset;
        $this->reset();
        return $str;
    }

    public function one(): ?array
    {
        $query = $this->build();
        $result =$this->db->sendQuery($query);
        return $this->db->one($result);
    }

    public function all(): ?array
    {
        $query = $this->build();
        $result = $this->db->sendQuery($query);
        return $this->db->all($result);
    }

    public function reset()
    {
        $this->columns = ['*'];
        $this->conditions = [];
        $this->table = null;
        $this->limit = '';
        $this->offset = '';
        $this->order = [];
    }
}


