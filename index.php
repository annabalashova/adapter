<?php
ini_set('display_errors',1);

include "../../vendor/autoload.php";

$connection = new \Packages\DbAdapter\MySQLConnection([
    'host' => 'localhost',
    'user' => 'root',
    'pass' => 'malish31',
    'db' => 'shop']);

$db = Packages\DbAdapter\Db::getInstance();

$db->setConnection($connection);

$queryBuilder = new \Packages\DbAdapter\QueryBuilder($db);
$result = $queryBuilder->table('products')
    ->select(['name', 'price'])
    ->where(['category_id' => '1'])
    ->order(['price' => 'ASC'])
    ->limit(5)
    ->offset(5)
    ->all();
print_r($result);


